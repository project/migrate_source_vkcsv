<?php

namespace Drupal\migrate_source_vkcsv\Plugin\migrate\source;

use Drupal\migrate\MigrateException;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_source_csv\Plugin\migrate\source\CSV;

/**
 * Source for CSV files.
 *
 * This migration source plugin extends the migrate_source_csv plugin to
 * provide for a virtual key in the migration.
 * The virtual key field SHOULD NOT exist in the source CSV file and is
 * constructed by combining one or more fields.
 *
 * Available configuration options:
 * - virtual_key_fields (required)
 *     Array of column names in the CSV file that can be combined to construct
 *     the virtual key.
 * - virtual_key_delimiter (optional. Default: _)
 *     The delimiter character to combine column values to form the key.
 * - virtual_key_default_values (optional)
 *     Associative array of default values to use if the corresponding
 *     column value is empty. The array is keyed by the column name.
 *
 * @example
 * Suppose we want to migrate a CSV with the following sample data:
 *
 * "First Name","Middle Name","Last Name","Degree","Specialties","NPI"
 * "Stephen","R.","Cann","MD","Psychiatry","1144237413"
 * "Michael","S.","Caplan","MD","Neonatal-Perinatal;Medicine;Pediatrics",""
 * "Michael","Charles","Caughron","MD","Internal Medicine;Medicine","1164534137"
 *
 * <code>
 * source:
 *   plugin: virtual_key_csv
 *   path: 'public://path/to/sourcefile.csv'
 *   track_changes: true
 *   ids:
 *     - key
 *   virtual_key_fields:
 *     - 'Last Name'
 *     - 'First Name'
 *     - NPI
 *   virtual_key_delimiter: _
 *   virtual_key_default_values:
 *     NPI: 0
 * </code>
 *
 * The above snippet will generate source ids as follows in the migration
 * lookup table:
 * cann_stephen_1144237413
 * caplan_michael_0
 * caughron_michael_1164534137
 *
 * @MigrateSource(
 *   id = "virtual_key_csv",
 *   source_module = "migrate_source_vkcsv"
 * )
 */
class VirtualKeyCSV extends CSV {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    if (count($configuration['ids']) > 1) {
      throw new MigrateException('The "ids" configuration must contain only 1 virtual key.');
    }

    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'virtual_key_fields' => [],
      'virtual_key_delimiter' => '_',
      'virtual_key_default_values' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getGenerator(\Iterator $records) {
    $record_num = $this->configuration['header_offset'] ?? 0;
    foreach ($records as $record) {
      if ($this->configuration['create_record_number']) {
        $record[$this->configuration['record_number_field']] = ++$record_num;
      }

      foreach ($this->configuration['ids'] as $id) {
        $record[$id] = $this->buildVirtualKeyValue($record);
      }

      yield $record;
    }
  }

  /**
   * Construct the virtual key.
   *
   * @param array $record
   *   A data row from the CSV source file.
   */
  protected function buildVirtualKeyValue(array $record) {
    $key = [];
    foreach ($this->configuration['virtual_key_fields'] as $field_label) {
      $value = $record[$field_label] ?? '';
      $value = $value ?: ($this->configuration['virtual_key_default_values'][$field_label] ?? '');
      $key[] = is_numeric($value) ? $value : self::sanitizeKey($value);
    }
    return implode($this->configuration['virtual_key_delimiter'], $key);
  }

  /**
   * Sanitize $value so that it can be used as a key.
   *
   * The returned value is lowercased and filename safe.
   *
   * @param string $value
   *   The key value to sanitize.
   *
   * @return string
   *   The sanitized key.
   */
  public static function sanitizeKey(string $value) {
    // Replace all spaces with hyphens.
    $value = str_replace(' ', '-', $value);

    // Remove special characters.
    $value = preg_replace('/[^A-Za-z0-9\-\_]/', '', $value);

    // Replace multiple hyphens with single one.
    $value = preg_replace('/-+/', '-', $value);

    return strtolower($value);
  }

}
